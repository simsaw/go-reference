package main

import (
	//"fmt"
	//"utility"
	//"interfaces"
	//"readers"
	//"routines"
	"channels"
	"routines"
)

func main() {
	/*
	str1, str2 := "Ben", "Cobb"
	fmt.Println(utility.ExportableFunction(str1, str2, 5))
	fmt.Println(utility.NamedReturnValues())
	fmt.Println(utility.UsingVar())
	fmt.Println(utility.ShortVariable(str1))
	fmt.Println(utility.FloatToInt(122.9))
	utility.PrintType(1.243)
	utility.Constants()

	fmt.Println("Multiply; ", utility.Multiply(5, 5))
	fmt.Println("Multiply; ", utility.MultiplyOptionalInitandPostStatements(5, 6))
	fmt.Println("Multiply; ", utility.While(5, 7))

	fmt.Println("Max: ", utility.Max(28, 22))
	fmt.Println("Is Greater: ", utility.IsGreater_IfElseWithInitializer(4,6))

	utility.WhichOS()
	utility.WhenIsSaturday()
	utility.SwitchWithoutCondition()

	utility.HelloWorldDeferred()
	fmt.Println(utility.DeferredStacking())

	//Pointers
	valToChange := 5
	pointerOfvalToChange := &valToChange
	utility.UpdatePointer(pointerOfvalToChange, 10)
	fmt.Println("Should be 15: ", valToChange)

	v := utility.Vertex{1,3}
	fmt.Printf("X: %v and Y: %v\n", v.X, v.Y)

	utility.StructPointerNoNeedToDereference(&v)
	fmt.Printf("New X: %v and New Y: %v\n", v.X, v.Y)
	utility.VariousDeclarations()

	utility.DeclareArrays()
	utility.Slice()
	utility.SliceRemoveLowandHighBounds()
	utility.SliceLenAndCap()
	utility.SliceNil()
	utility.DynamicArrays_MakeSlices()
	utility.SlicesInSlices()
	utility.AppendSlices()
	utility.SliceRange()

	utility.MapIntro()
	utility.MapLiterals()
	utility.MapLiterals_Shortened()
	utility.MapMutation()

	utility.FunctionValues()
	utility.FunctionClosures()

	utility.Method()
	vert := utility.Vert{45,3}
	fmt.Println(vert.Mult())
	fmt.Println("Scalar, vert remains the same: ", vert)
	vert.Scale(2)
	fmt.Println("Pointer, vert has been mutated: ", vert)
	var methodFloat utility.MyFloat = -4.5
	fmt.Println(methodFloat.Abs())

	//Interfaces
	interfaces.InterfaceTest()
	interfaces.InterfaceNil()
	interfaces.InterfaceEmpty()
	interfaces.InterfaceTypeAssertion()
	interfaces.TypeSwitches(21)
	interfaces.TypeSwitches("yo")
	interfaces.TypeSwitches(true)
	interfaces.Stringer()

	interfaces.ErrorRun2()

	readers.ReaderStrings()

	routines.HelloWorldGoRoutine()
	*/
	routines.Mutex1()

	channels.Channel()
	channels.Channel_RangeClose()
	channels.Channel_Select()
}