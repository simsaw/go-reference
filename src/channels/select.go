package channels


/*
The select statement lets a goroutine wait on multiple communication operations.
A select blocks until one of its cases can run, then it executes that case. It chooses one at random if multiple are ready.
 */

import (
	"fmt"
	"time"
)

func fibonacci(c, quit chan int) {
	x, y := 0, 1
	for { //infinite loop
		select {
		case c <- x: //I assume this gets called every time
			x, y = y, x+y
		case <-quit: //once quit is pushed to, this case fires
			fmt.Println("quit")
			return
		}
	}
}

func Channel_Select() {
	fib()
	tickingTime()
}

func fib(){
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println(<-c)  //this lies waiting until a value is pushed to c
		}
		quit <- 0 //once we have gone through the for loop, we push to quit
	}()
	fibonacci(c, quit)
}


func tickingTime() {
	tick := time.Tick(1000 * time.Millisecond)
	boom := time.After(5000 * time.Millisecond)
	for {
		select {
		case <-tick:
			fmt.Println("tick.")
		case <-boom:
			fmt.Println("BOOM!")
			return
		default:
			fmt.Println("    .")
			time.Sleep(200 * time.Millisecond)
		}
	}
}