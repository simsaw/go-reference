package testframework

import "testing"

func TestExportable(t *testing.T) {
	v := Exportable()
	if v != "hi" {
		t.Errorf("Expected hi, got: %v", v)
	}
}