package utility

import "fmt"

var a bool
var b, c bool = true, false
var f, g = false, false //without type declaration, takes the type of the initializer

const Pi = 3.14 //immutable, can be added to funcs as well
const (
	Big = 1 << 100 //binary 1 followed by 100 zeroes
	Small = Big >> 99 // 01 binary => 2
)

func Constants (){
	const Pi2 = 3.1415
	fmt.Println(Pi, float64(Big))
}

func ExportableFunction(str1, str2 string, int1 int) (string, int){
	return "Hello " + str1 + " " + str2, int1*2
}

func FloatToInt(float1 float64) (ret int64){
	ret = int64(float1)
	return
}

func NamedReturnValues() (x, y int){
	x = 14
	y = x*3
	return //naked returns, only use in short/small functions, loses readability in larger functions
}

func UsingVar() (one, two bool){
	var d = true
	var e bool
	one = a && b && c && e
	two = d || a || f || g
	return
}

func ShortVariable (str string) (ret bool, ret2 string){
	d := true //only available inside func, can be used in place of a var with implicit type
	ret = d
	ret2 = "Yo, Yo " + str
	return
}

func PrintType (float float64){
	fmt.Printf("Type = %T\n", float)
}
func privateFunction() string {
	return "it's all about the captialization of the first letter of the function name"
}