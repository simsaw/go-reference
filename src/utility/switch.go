package utility

import (
	"fmt"
	"runtime"
	"time"
)
/*
A case body breaks automatically, unless it ends with a fallthrough statement.

Switch cases evaluate cases from top to bottom, stopping when a case succeeds

Switch without a condition is the same as switch true.
This construct can be a clean way to write long if-then-else chains.

 */


func WhichOS() {
	fmt.Print("Running on ")
	switch os := runtime.GOOS; os {
	case "darwin":
		fmt.Println("OS X.")
	case "linux":
		fmt.Println("Linux.")
	default:
		fmt.Printf("%s.", os)
	}
}

func WhenIsSaturday() {
	fmt.Println("When's Saturday?")
	today := time.Now().Weekday()
	switch time.Saturday {
	case today + 0:
		fmt.Println("Today.")
	case today + 1:
		fmt.Println("Tomorrow.")
	case today + 2:
		fmt.Println("In two days.")
	case today + 3:
		fmt.Println("In three days.")
	default:
		fmt.Println("Too far away.")
	}
}

func SwitchWithoutCondition(){
	t := time.Now()
	switch {
	case t.Hour() < 12:
		fmt.Println("Good morning!")
	case t.Hour() < 17:
		fmt.Println("Good afternoon.")
	default:
		fmt.Println("Good evening.")
	}
}