package utility

/*
A map maps keys to values.
The zero value of a map is nil. A nil map has no keys, nor can keys be added.
The make function returns a map of the given type, initialized and ready for use.

Map literals are like struct literals, but the keys are required.

Mutating Maps
Insert or update an element in map m:
m[key] = elem

Retrieve an element:
elem = m[key]

Delete an element:
delete(m, key)

Test that a key is present with a two-value assignment:
elem, ok = m[key]
If key is in m, ok is true. If not, ok is false.
If key is not in the map, then elem is the zero value for the map's element type.
 */

import "fmt"

type Geocode struct {
	Lat, Long float64
}

var m map[string]Geocode

func MapIntro() {
	m = make(map[string]Geocode)
	m["Bell Labs"] = Geocode{
		40.68433, -74.39967,
	}
	m["Simsaw"] = Geocode{
		40, -90,
	}
	fmt.Println(m)
}

func MapLiterals(){
	m := map[string]Geocode{
		"Bell Labs": Geocode{
		40.68433, -74.39967,
		},
		"Simsaw": Geocode{
		40, -90,
		},
	}

	fmt.Println(m)
}

func MapLiterals_Shortened(){ //If the top-level type is just a type name, you can omit it from the elements of the literal.
	m := map[string]Geocode{
		"Bell Labs": {40.68433, -74.39967},
		"Simsaw": {40, -90 },
	}

	fmt.Println(m)
}

func MapMutation(){
	m := make(map[string]int)

	m["Answer"] = 42
	fmt.Println("The value:", m["Answer"])

	m["Answer"] = 48
	fmt.Println("The value:", m["Answer"])

	delete(m, "Answer")
	fmt.Println("The value:", m["Answer"])

	v, ok := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok)
}