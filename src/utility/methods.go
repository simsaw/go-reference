package utility

import (
	"fmt"
	"math"
)

/*
Go does not have classes. However, you can define methods on types.
A method is a function with a special receiver argument.
The receiver appears in its own argument list between the func keyword and the method name.
In this example, the Abs method has a receiver of type Vertex named v.

Remember: a method is just a function with a receiver argument.

You can declare a method on non-struct types, too.
In this example we see a numeric type MyFloat with an Abs method.
You can only declare a method with a receiver whose type is defined in the same package as the method. You cannot
declare a method with a receiver whose type is defined in another package (which includes the built-in types such as int).

Pointer receivers are more common than value receivers
You can declare methods with pointer receivers.
This means the receiver type has the literal syntax *T for some type T. (Also, T cannot itself be a pointer such as *int.)
For example, the Scale method here is defined on *Vertex.
Methods with pointer receivers can modify the value to which the receiver points
Note: For the statement v.Scale(5), even though v is a value and not a pointer, the method with the pointer receiver is called automatically.
That is, as a convenience, Go interprets the statement v.Scale(5) as (&v).Scale(5) since the Scale method has a pointer receiver.
Works in the opposite direction
p := &v
p.Abs() //even though p is a pointer, and the receiver is a value, it will do (*p).Abs() automatically

There are two reasons to use a pointer receiver.
The first is so that the method can modify the value that its receiver points to.
The second is to avoid copying the value on each method call. This can be more efficient if the receiver is a large struct, for example.
In this example, both Scale and Abs are with receiver type *Vertex, even though the Abs method needn't modify its receiver.
NOTE: In general, all methods on a given type should have either value or pointer receivers, but not a mixture of both.
 */

type Vert struct {
	X, Y float64
}

func (v Vert) abs() float64 { //this reminds me of extensions in C#
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v Vert) Mult() float64 {
	return v.X * v.Y
}

func (v *Vert) Scale(f float64){
	v.X = v.X*f
	v.Y = v.Y*f
}

type MyFloat float64

func (f MyFloat) Abs() float64 {
	if f < 0 {
		return float64(-f)
	}
	return float64(f)
}

func Method() {
	v := Vert{3, 4}
	fmt.Println(v.abs())
	fmt.Println(v.Mult())
}