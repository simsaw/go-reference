package utility

func Max (val1, val2 int) (max int){
	max = val1

	if val1 < val2{
		max = val2
	}

	return
}

func IsGreater_IfElseWithInitializer (val1, val2 int) bool{
	//scope of the initialized value is only in the if statement
	if a := true; val1 > val2{
		return a
	} else {
		return !a
	}
}