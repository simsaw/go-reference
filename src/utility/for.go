package utility

/*
Go has only one looping construct, the for loop.

The basic for loop has three components separated by semicolons:

the init statement: executed before the first iteration
the condition expression: evaluated before every iteration
the post statement: executed at the end of every iteration

The init statement will often be a short variable declaration, and the variables declared there are visible only in the scope of the for statement.

The loop will stop iterating once the boolean condition evaluates to false.

Note: Unlike other languages like C, Java, or Javascript there are no parentheses surrounding the three components of the for statement and the braces { } are always required.
 */

func Multiply (num, times int) (final int){
	for i := 0; i < times; i++ {
		final += num
	}

	return
}

func MultiplyOptionalInitandPostStatements (num, times int) (final int){
	i := 0
	for ; i < times ;{ //essentially a while loop, can remove the semicolons and become a While statement
		final += num
		i++
	}
	return
}

func While (num, times int) (final int){
	inc := 0
	for inc < times {
		final += num
		inc++
	}

	return
}

func InfiniteLoop (){
	for {
	}
}