package utility

import "fmt"

/*
A struct is a collection of fields.

Struct fields can be accessed through a struct pointer.
To access the field X of a struct when we have the struct pointer p we could write (*p).X.
However, that notation is cumbersome, so the language permits us instead to write just p.X,
without the explicit dereference.
 */


type Vertex struct {
	X int
	Y int
}

type vertex struct { //private struct, not exportable due to the lowercase first letter, just like functions
	X int
	Y int
}

func StructPointerNoNeedToDereference (v *Vertex){
	//v is a pointer to a Vertex struct, however, I can simply call v.X to manipulate, no need to (*v).X
	v.X = 23
	v.Y = 34
}

func VariousDeclarations(){
	var(
		v1 = Vertex{1, 2}  // has type Vertex
		v2 = Vertex{X: 1}  // Y:0 is implicit
		v3 = Vertex{}      // X:0 and Y:0
		p  = &Vertex{1, 2} // has type *Vertex
	)

	fmt.Println(v1, v2, v3, p)
}
