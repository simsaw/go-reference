package utility

import (
	"fmt"
)

/*
A defer statement defers the execution of a function until the surrounding function returns.
The deferred call's arguments are evaluated immediately, but the function call is not executed until the surrounding function returns.

Stacking Defers
Deferred function calls are pushed onto a stack. When a function returns, its deferred calls are executed in last-in-first-out order.
 */

func HelloWorldDeferred() {
	defer fmt.Println("world")
	fmt.Print("hello ")
}

func DeferredStacking() string{
	fmt.Println("counting")
	for i := 0; i < 10; i++{
		defer fmt.Printf("%v ", i)
	}

	fmt.Println("done")

	return "Returned - Finally"
}