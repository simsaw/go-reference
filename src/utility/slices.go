package utility

import (
	"fmt"
	"strings"
)

/*
The type [n]T is an array of n values of type T.

An array's length is part of its type, so arrays cannot be resized. This seems limiting,
but don't worry; Go provides a convenient way of working with arrays.

Slice
An array has a fixed size. A slice, on the other hand, is a dynamically-sized, flexible view into the elements of an array.
In practice, slices are much more common than arrays.
The type []T is a slice with elements of type T.
A slice does not store any data, it just describes a section of an underlying array.
Changing the elements of a slice modifies the corresponding elements of its underlying array.
Other slices that share the same underlying array will see those changes

Length and Capacity
A slice has both a length and a capacity.
The length of a slice is the number of elements it contains.
The capacity of a slice is the number of elements in the underlying array, counting from the first element in the slice.
The length and capacity of a slice s can be obtained using the expressions len(s) and cap(s).
You can extend a slice's length by re-slicing it, provided it has sufficient capacity.

Nil Slice
The zero value of a slice is nil.
A nil slice has a length and capacity of 0 and has no underlying array.

Dynamic Arrays
Slices can be created with the built-in make function; this is how you create dynamically-sized arrays.

Slices of Slices
Slices can contain any type, including other slices.

Appending Slice (growing the array)
It is common to append new elements to a slice, and so Go provides a built-in append function. The documentation of the built-in package describes append.
func append(s []T, vs ...T) []T
The first parameter s of append is a slice of type T, and the rest are T values to append to the slice.
The resulting value of append is a slice containing all the elements of the original slice plus the provided values.
If the backing array of s is too small to fit all the given values a bigger array will be allocated. The returned slice will point to the newly allocated array.

Slice Range
The range form of the for loop iterates over a slice or map.
When ranging over a slice, two values are returned for each iteration. The first is the index, and the second is a copy of the element at that index.
 */

var(
	strArr[2]string
)

func DeclareArrays(){
	strArr[0] = "Hello"
	strArr[1] = "World"

	primes := [6]int{2, 3, 5, 7, 11, 13}

	fmt.Println(strArr, primes)
}

func Slice(){
	primes := [6]int{2, 3, 5, 7, 11, 13}
	var s []int = primes[2:4] //2 - would be 5, and it goes to position 4, without going there, so result will be [5,7]

	fmt.Println(s)
}

func SliceLiteral(){
	//A slice literal is like an array literal without the length.
	//An array is generated, with that size, and the slice is created and that is what the variable is

	q := []int{2, 3, 5, 7, 11, 13}
	fmt.Println(q)

	r := []bool{true, false, true, true, false, true}
	fmt.Println(r)

	s := []struct { //an array of struct, instantiated all together
		i int
		b bool
	}{
		{2, true},
		{3, false},
		{5, true},
		{7, true},
		{11, false},
		{13, true},
	}
	fmt.Println(s)
}

func SliceRemoveLowandHighBounds(){
	s := []int{2, 3, 5, 7, 11, 13}

	s = s[1:4]
	fmt.Print(s) //[3,5,7]

	s = s[:2]
	fmt.Print(s) //[3,5]

	s = s[1:]
	fmt.Println(s) //[5]
}

func SliceLenAndCap(){
	s := []int{2, 3, 5, 7, 11, 13}

	s = s[:0] //0 to 0, will be empty, however the cap is the underlying size, so I can still extend the array, if there is cap
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	s = s[:4]
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	s = s[1:4]
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)

	//s = s[:8] // this will error out, 8 > cap(s)

}

func SliceNil(){
	var s[]int
	fmt.Println(s, len(s), cap(s))
	if s == nil {
		fmt.Println("nil!")
	}
}

func DynamicArrays_MakeSlices(){
	a := make([]int, 5)
	printSlice("a", a)

	b := make([]int, 0, 5)
	printSlice("b", b)

	c := b[:2]
	printSlice("c", c)

	d := c[2:5]
	printSlice("d", d)
}

func SlicesInSlices(){
	// Create a tic-tac-toe board.
	board := [][]string{
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
		[]string{"_", "_", "_"},
	}

	// The players take turns.
	board[0][0] = "X"
	board[2][2] = "O"
	board[1][2] = "X"
	board[1][0] = "O"
	board[0][2] = "X"

	for i := 0; i < len(board); i++ {
		fmt.Printf("%s\n", strings.Join(board[i], " "))
	}
}

func AppendSlices(){
	var s []int
	printSlice2(s)

	// append works on nil slices.
	s = append(s, 0)
	printSlice2(s)

	// The slice grows as needed.
	s = append(s, 1)
	printSlice2(s)

	// We can add more than one element at a time.
	s = append(s, 2, 3, 4)
	printSlice2(s)
}

func SliceRange(){
	pow := []int{1, 2, 4, 8, 16, 32, 64, 128}
	for i,v := range pow { //i is the index, v is the value
		fmt.Printf("2**%d = %d\n", i, v)
	}

	pow = make([]int, 10)
	for i := range pow { //skip v
		pow[i] = 1 << uint(i) // == 2**i
	}
	for _, value := range pow { //skip index with _
		fmt.Printf("%d\n", value)
	}
}

func printSlice2(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func printSlice(s string, x []int) {
	fmt.Printf("%s len=%d cap=%d %v\n",
		s, len(x), cap(x), x)
}

