package readers

import (
	"fmt"
	"io"
	"strings"
)

func ReaderStrings() {
	r := strings.NewReader("Hello, Reader!")

	b := make([]byte, 8)
	for {
		n, err := r.Read(b)
		fmt.Printf("n = %v err = %v b = %v\n", n, err, b)
		fmt.Printf("b[:n] = %q\n", b[:n])
		if err == io.EOF {
			break
		}
	}
}

/*
My own Reader...
Infinitely fill the array with 'A'
 */
type MyReader struct{}

func (r MyReader) Read(arr []byte) (int, error){
	for i := 0; i < len(arr); i++{
		arr[i] = 'A'
	}

	return len(arr), nil
}
